import requests
import re
from bs4 import BeautifulSoup

KEY = "glpat-AB4qbv6YxzAvV8Gx8LSW"

import requests

from bs4 import BeautifulSoup


def get_newest_ret():
    url = "https://www.cfmem.com/search/label/free"
    resp = requests.get(url)

    soup = BeautifulSoup(resp.content, "html.parser")
    h2 = soup.find("h2", class_="entry-title")
    a = h2.find("a")
    href = a.get("href")

    resp = requests.get(href)
    soup = BeautifulSoup(resp.content, "html.parser")
    h2_list = soup.find_all("h2")

    pre_h2 = None
    for h2 in h2_list:
        if h2.text == "订阅链接":
            pre_h2 = h2
    if not pre_h2:
        return

    dd = pre_h2.find_next_sibling("div")
    dd = dd.find_all("div")
    ret = {}
    for d in dd:
        d_text = d.text
        n, v = d_text.split("->")
        ret[n.strip()] = v.strip()
    return ret


def get_list_page():
    url = "https://www.cfmem.com/search/label/free"
    soup = BeautifulSoup(requests.get(url).text, "html.parser")
    durl = soup.find(name="h2").find("a").get("href")

    soup = BeautifulSoup(requests.get(durl).text, "html.parser")
    ret_list = soup.find("h2", string=re.compile(r"订阅链接")).next_sibling.find_all("div")

    ret_map = {}
    for ret in ret_list:
        n, u = ret.text.split("：")
        if n and u:
            ret_map[n] = u
    return ret_map


def upload_one(name: str, file_url: str):
    content = requests.get(file_url).text
    url = f"https://gitlab.com/api/v4/projects/42641795/repository/files/{name}?private_token={KEY}"

    resp = requests.delete(url=url, json={"branch": "main", "commit_message": "update", "content": content})
    resp = requests.post(url=url, json={"branch": "main", "commit_message": "update", "content": content})
    return resp.json()


def get_list_page_2():
    try:
        prefix = "clashnode"
        url = "https://clashnode.com/"
        soup = BeautifulSoup(requests.get(url).text, "html.parser")
        durl = soup.find("ul", {"class": "post-list"}).find("a").get("href")

        soup = BeautifulSoup(requests.get(durl).text, "html.parser")
        p_list = soup.find("h2", string=re.compile(r"订阅链接")).find_next_siblings('p')

        return {
            f"{prefix}_{p_list[1].text}": p_list[2].text,
            f"{prefix}_{p_list[3].text}": p_list[4].text,
        }
    except Exception as e:
        print(e)
        return {}


def main():
    ret_map = get_newest_ret()
    print(f"{ret_map = }")
    for name, url in ret_map.items():
        print(upload_one(name.replace("/", ""), url))


if __name__ == "__main__":
    main()
